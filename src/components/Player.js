import React from 'react'
import PropTypes from 'prop-types'
import pure from 'recompose/pure'
import { Loader, Cards } from './index'


function Player({ id, player, score, click, isBot }) {
  return (
    <div className='player'>
      <h1>{id}</h1>
      <div>Score:{score ? score : 0}</div>
      {player && player.isLoading && <Loader />}
      {player && player.cards && <Cards cards={player.cards} onclick={click} isBot={isBot} />}
    </div>
  )
}

Player.propTypes = {
  id: PropTypes.string.isRequired,
  player: PropTypes.object,
  score: PropTypes.number,
  isBot: PropTypes.bool
}

export default pure(Player)