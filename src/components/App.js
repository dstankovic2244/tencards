import React from 'react'
import PropTypes from 'prop-types'
import pure from 'recompose/pure'
import { Loader } from './index'
import { PlayerContainer, PileContainer } from '../containers/index'

function App({ deck, humanId, bots, selectNumPlayers, numPlayers }) {
  const deckFlag = deck && deck.deckConfig && deck.deckConfig.deck_id !== undefined;
  return (
    <div>
      {!numPlayers ?
        <div className="main">
          <div>Select number of players:</div>
          <button className="numPlayers" onClick={selectNumPlayers.bind(this, 2)}>2 players</button><br />
          <button className="numPlayers" onClick={selectNumPlayers.bind(this, 3)}>3 players</button><br />
          <button className="numPlayers" onClick={selectNumPlayers.bind(this, 4)}>4 players</button><br />
        </div>
        :
        null
      }
      {deck && deck.isLoading && <Loader />}
      <div className="col-12">{deckFlag && (numPlayers === 4) && <PlayerContainer id="Rosetta" deckConfig={deck.deckConfig} isBot={true} />}</div>
      <div className="row" >
        <div className="col-5">{deckFlag && (numPlayers >= 2) && <PlayerContainer id="Ola" deckConfig={deck.deckConfig} isBot={true} />}</div>
        <div className='col-2'>{deckFlag && <PileContainer id="pile" />}</div>
        <div className="col-5">{deckFlag && (numPlayers >= 3) && <PlayerContainer id="Carolyn" deckConfig={deck.deckConfig} isBot={true} />}</div>
      </div>

      <div className="col-12">{deckFlag && <PlayerContainer id={humanId} deckConfig={deck.deckConfig} bots={bots} />}</div>
    </div>
  )
}

App.propTypes = {
  deck: PropTypes.object,
  humanId: PropTypes.string,
  bots: PropTypes.array,
  selectNumPlayers: PropTypes.func,
  numPlayers: PropTypes.number,
}

export default pure(App)