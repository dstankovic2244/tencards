import React from 'react'
import PropTypes from 'prop-types'
import pure from 'recompose/pure'

import { Cards } from './index'


function Pile({ pile }) {
  return (
    <div className="player">
      {pile && pile.cards && <Cards cards={pile.cards} />}
    </div>
  )
}

Pile.propTypes = {
  pile: PropTypes.object.isRequired
}

export default pure(Pile)