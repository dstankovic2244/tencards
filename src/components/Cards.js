import React from 'react'
import PropTypes from 'prop-types'
import pure from 'recompose/pure'


function Cards({ cards, isBot, onclick }) {

  return (
    <div >
      {cards.map((card, index) => {
        if (isBot) {
          return <img src="/img/cardback.jpg" key={index} alt={card.value} className="card bot" />
        }
        else {
          return <img src={card.image} key={index} alt={card.value} className="card" onClick={onclick ? onclick.bind(this, card) : null} />
        }
      })}
    </div>
  )
}

Cards.propTypes = {
  cards: PropTypes.array,
  onclick: PropTypes.func,
  isBot: PropTypes.bool
}

export default pure(Cards)