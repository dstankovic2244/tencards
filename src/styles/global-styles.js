import { injectGlobal } from 'styled-components'

/* eslint no-unused-expressions: 0 */
injectGlobal`
  html,
  body {
    height: 100%;
    width: 100%;
  }

  body {
    margin: 0;
    padding: 0;
    font-family: sans-serif;
    background: url(/img/bckg.jpg);
    color: #fff;
  }
  img.card {
    max-width: 75px;
    cursor: pointer;
  }
  .bot {
    margin-right: -90px;
  }
  .loader {
    display: flex;
    height: 50vh;
    justify-content: center;
    align-items: center;
  }
  
  .spinner {
    height: 5vh;
    width: 5vh;
    border: 6px solid rgba(0, 174, 239, 0.2);
    border-top-color: rgba(0, 174, 239, 0.8);
    border-radius: 100%;
    animation: rotation 0.6s infinite linear 0.25s;
  
    /* the opacity is used to lazyload the spinner, see animation delay */
    /* this avoid the spinner to be displayed when visible for a very short period of time */
    opacity: 0;
  }
  
  @keyframes rotation {
    from {
      opacity: 1;
      transform: rotate(0deg);
    }
    to {
      opacity: 1;
      transform: rotate(359deg);
    }
  }
  * {
    box-sizing: border-box;
}

[class*="col-"] {
    float: left;
    padding-top: 10px;
    padding-right:10px;
    padding-bottom:10px;
    padding-left:10px;
}
.row::after {
    content: "";
    clear: both;
    display: block;
}
.player{
  padding:10px;
  max-height:200px;
}

    .main {
        min-height: 600px;
        padding-top: 300px;
        font-size: 30px

    }

/* For mobile phones: */
[class*="col-"] {
    width: 100%;
}
@media only screen and (min-width: 768px) {
    /* For desktop: */
    .col-1 {width: 8.33%;}
    .col-2 {width: 16.66%;}
    .col-3 {width: 33.33%;}
    .col-4 {width: 33.33%;}
    .col-5 {width: 41.66%;}
    .col-6 {width: 50%;}
    .col-7 {width: 58.33%;}
    .col-8 {width: 66.66%;}
    .col-9 {width: 75%;}
    .col-10 {width: 83.33%;}
    .col-11 {width: 91.66%;}
    .col-12 {width: 100%;}
}
.numPlayers{
  height:30px;
  width:400px;
  font-size:20px;
}
`
