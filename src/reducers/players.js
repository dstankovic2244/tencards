import { EDIT_PLAYER, PLAYER_CARDS_REMOVE, PLAYERS_SET_SCORE } from 'constants/ActionTypes'

const initialState = {
  'me': { isLoading: false },
  'pile': { cards: [] }
}

function editPlayer(state, action) {
  const { payload } = action;
  const { id } = payload;
  let newPlayerData = { ...payload };
  const oldPlayerData = state[id];
  delete newPlayerData[id];

  return {
    ...state,
    [id]: {
      ...oldPlayerData,
      ...newPlayerData
    }
  }
}

export default function players(state = initialState, action) {
  switch (action.type) {
    case EDIT_PLAYER:
      return editPlayer(state, action);

    case PLAYERS_SET_SCORE:
      action.payload.score = (state[action.payload.id].score ? state[action.payload.id].score : 0) + action.payload.score;
      return editPlayer(state, action)
    case PLAYER_CARDS_REMOVE:
      let cards = state[action.payload.id].cards.filter(obj => obj.image !== action.payload.card.image);
      delete action.payload.card;
      action.payload.cards = cards;
      return editPlayer(state, action);
    default:
      return state
  }
}