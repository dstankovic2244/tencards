import { FETCH_DECK_REQUEST, FETCH_DECK_FAILED, FETCH_DECK_SUCCESS } from 'constants/ActionTypes'

const initialState = { deckConfig: undefined, isLoading: false, error: undefined }

export default function deck(state = initialState, action) {
  switch (action.type) {
    case FETCH_DECK_REQUEST:
      return Object.assign({}, state, { isLoading: true })
    case FETCH_DECK_FAILED:
      return Object.assign({}, state, { isLoading: false, error: action.payload })
    case FETCH_DECK_SUCCESS:
      return Object.assign({}, state, { isLoading: false, deckConfig: action.payload })
    default:
      return state
  }
}
