import { combineReducers } from 'redux'
import deck from './deck'
import players from './players'

const rootReducer = combineReducers({
  deck,
  players
})

export default rootReducer
