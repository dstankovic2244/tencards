export const FETCH_DECK_REQUEST = 'FETCH_DECK_REQUEST'
export const FETCH_DECK_FAILED = 'FETCH_DECK_FAILED'
export const FETCH_DECK_SUCCESS = 'FETCH_DECK_SUCCESS'

export const EDIT_PLAYER = 'EDIT_PLAYER'
export const PLAYER_CARDS_REMOVE = 'PLAYER_CARDS_REMOVE'
export const PLAYERS_SET_SCORE = 'PLAYERS_SET_SCORE'



