import { EDIT_PLAYER, PLAYER_CARDS_REMOVE, PLAYERS_SET_SCORE } from 'constants/ActionTypes'
import { createAction } from 'redux-actions'

export const editPlayer = createAction(EDIT_PLAYER)

export const playerCardsRemove = createAction(PLAYER_CARDS_REMOVE)

export const playerSetScore = createAction(PLAYERS_SET_SCORE)

export function addCardToPile(payload) {
  return (dispatch, getState) => {
    const { players } = getState()

    let cards = players['pile'].cards ? players['pile'].cards.slice() : []
    cards.push(Object.assign({}, payload.card, { id: payload.id }))
    dispatch(editPlayer({ id: 'pile', cards }))
  }
}

export function emptyPile() {
  return (dispatch, getState) => {
    const { players } = getState()
    const pileCards = players.pile.cards
    let sum = 0
    let maxCard = null

    pileCards.forEach((card) => {
      sum += parseInt(cardValue(card), 10)
      if (!maxCard) {
        maxCard = card
      } else if (cardValue(maxCard) <= cardValue(card)) {
        maxCard = card
      }
    })
    dispatch(playerSetScore({ id: maxCard.id, score: sum }))
    setTimeout(function() {
      dispatch(editPlayer({ id: 'pile', cards: [] }))
    }, 2000)
  }
}

function cardValue(card) {
  if (!card) {
    return 0
  }
  switch (card.value) {
    case 'KING':
      return 14
    case 'QUEEN':
      return 13
    case 'JACK':
      return 12
    case 'ACE':
      return 1
    default:
      return card.value
  }
}
