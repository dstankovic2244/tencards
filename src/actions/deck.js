import { FETCH_DECK_REQUEST, FETCH_DECK_FAILED, FETCH_DECK_SUCCESS } from 'constants/ActionTypes'
import { createAction } from 'redux-actions'

export const fetchDeckRequest = createAction(FETCH_DECK_REQUEST)

export const requestFailed = createAction(FETCH_DECK_FAILED)

export const requestSuccesed = createAction(FETCH_DECK_SUCCESS)