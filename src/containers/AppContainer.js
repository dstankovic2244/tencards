import React from 'react'
import PropTypes from 'prop-types'
import { App } from 'components'

import { createStructuredSelector, createSelector } from 'reselect'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as DeckActions from 'actions/deck'
import axios from 'axios';

class AppContainer extends React.Component {
  constructor(props) {
    super(props)
    this.state = { numPlayers: 0 };
    this.humanId = "me";
    this.bots = ["Rosetta", "Ola", "Carolyn"];
    this.selectNumPlayers = this.selectNumPlayers.bind(this);
  }
  static propTypes = {
    fetchDeckRequest: PropTypes.func.isRequired,
    requestFailed: PropTypes.func.isRequired,
    requestSuccesed: PropTypes.func.isRequired,
    deck: PropTypes.object
  }
  async selectNumPlayers(num) {
    this.props.fetchDeckRequest();
    try {
      const response = await axios.get(`https://deckofcardsapi.com/api/deck/new/shuffle/?deckd_count=1`)
      this.props.requestSuccesed(response.data);
    }
    catch (e) {
      this.props.requestFailed(e);
    }
    this.setState({
      numPlayers: num
    });
  }
  fetchDeckRequest = () => {
    this.props.fetchDeckRequest()
  }

  requestFailed = () => {
    this.props.requestFailed()
  }

  requestSuccesed = (data) => {
    this.props.requestSuccesed(data)
  }

  render() {
    return (
      <App
        deck={this.props.deck}
        humanId={this.humanId}
        bots={this.bots}
        selectNumPlayers={this.selectNumPlayers}
        numPlayers={this.state.numPlayers}
      />
    )
  }
}

const mapStateToProps = createStructuredSelector({
  deck: createSelector(
    (state) => state.deck,
    (deckState) => deckState
  )
})

function mapDispatchToProps(dispatch) {
  return bindActionCreators(DeckActions, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(AppContainer)