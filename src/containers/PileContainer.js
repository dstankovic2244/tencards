import React from 'react'
import PropTypes from 'prop-types'
import { Pile } from 'components'

import { createStructuredSelector, createSelector } from 'reselect'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as PlayerActions from 'actions/players'

class PileContainer extends React.Component {
  static propTypes = {
    id: PropTypes.string.isRequired,
    players: PropTypes.object
  }

  render() {
    return (
      <Pile
        pile={this.props.players[this.props.id]}
      />
    )
  }
}

const mapStateToProps = createStructuredSelector({
  players: createSelector(
    (state) => state.players,
    (playersState) => playersState
  )
})

function mapDispatchToProps(dispatch) {
  return bindActionCreators(PlayerActions, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(PileContainer)