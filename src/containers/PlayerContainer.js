import React from 'react'
import PropTypes from 'prop-types'
import { Player } from 'components'

import { createStructuredSelector, createSelector } from 'reselect'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as PlayerActions from 'actions/players'
import axios from 'axios';

class PlayerContainer extends React.Component {
  constructor(props) {
    super(props)

    this.onCardClick = this.onCardClick.bind(this);
  }
  static propTypes = {
    editPlayer: PropTypes.func.isRequired,
    playerCardsRemove: PropTypes.func.isRequired,
    addCardToPile: PropTypes.func.isRequired,
    id: PropTypes.string.isRequired,
    players: PropTypes.object,
    isBot: PropTypes.bool,
    bots: PropTypes.array
  }
  async componentDidMount() {
    this.props.editPlayer({ id: this.props.id, isLoading: true });
    try {
      const response = await axios.get(`https://deckofcardsapi.com/api/deck/${this.props.deckConfig.deck_id}/draw/?count=10`)
      this.props.editPlayer({ id: this.props.id, isLoading: false, cards: response.data.cards });
    }
    catch (e) {
      this.props.editPlayer({ id: this.props.id, error: e });
    }
  }
  onCardClick(card) {

    const self = this;
    if (this.props.players.pile.cards && this.props.players.pile.cards.length) {
      return;
    }
    this.removeAndAddCardToPile({ id: this.props.id, card });
    let botPlayers = Object.keys(this.props.players).filter(playerId => self.props.bots.includes(playerId));

    botPlayers.map(
      (botId) => {
        const randindex = Math.floor(Math.random() * Math.floor(self.props.players[botId].cards.length - 1));
        const botRandCard = self.props.players[botId].cards[randindex];
        self.removeAndAddCardToPile({ id: botId, card: botRandCard });
        return true;
      }
    );
    this.props.emptyPile();
  }

  removeAndAddCardToPile(data) {
    this.props.playerCardsRemove({ ...data });
    this.props.addCardToPile(data);
  }

  render() {
    return (
      <Player
        id={this.props.id}
        player={this.props.players[this.props.id]}
        score={this.props.players[this.props.id] ? this.props.players[this.props.id].score : 0}
        click={!this.props.isBot ? this.onCardClick : null}
        isBot={this.props.isBot}
      />
    )
  }
}

const mapStateToProps = createStructuredSelector({
  players: createSelector(
    (state) => state.players,
    (playersState) => playersState
  )
})

function mapDispatchToProps(dispatch) {
  return bindActionCreators(PlayerActions, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(PlayerContainer)